<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_devoir' => 'Ajouter ce devoir',

	// I
	'icone_creer_devoir' => 'Créer un devoir',
	'icone_supprimer_devoir' => 'Supprimer ce devoir',
	'icone_modifier_devoir' => 'Modifier ce devoir',
	'info_1_devoir' => 'Un devoir',
	'info_aucun_devoir' => 'Aucun devoir',
	'info_devoirs_auteur' => 'Les devoirs de cet auteur',
	'info_nb_devoirs' => '@nb@ devoirs',

	// L
	'label_date_echeance' => 'Pour le',
	'label_doc_joint' => 'Document joint',
	'label_doc_joints' => 'Documents joints',
	'label_id_classe' => 'Classe',
	'label_matiere' => 'Matière',
	'label_texte' => 'Texte',

	// R
	'retirer_lien_devoir' => 'Retirer ce devoir',
	'retirer_tous_liens_devoirs' => 'Retirer tous les devoirs',

	// T
	'texte_ajouter_devoir' => 'Ajouter un devoir',
	'texte_changer_statut_devoir' => 'Ce devoir est :',
	'texte_creer_associer_devoir' => 'Créer et associer un devoir',
	'titre_devoir' => 'Devoir',
	'titre_devoirs' => 'Devoirs',
	'titre_devoirs_rubrique' => 'Devoirs de la rubrique',
	'titre_langue_devoir' => 'Langue de ce devoir',
	'titre_logo_devoir' => 'Logo de ce devoir',
);

?>