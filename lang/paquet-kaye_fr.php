<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// K
	'kaye_description' => 'Un cahier de texte électronique pour l’école primaire.',
	'kaye_nom' => 'kaye',
	'kaye_slogan' => 'Un cahier de texte électronique pour l’école primaire.',
);

?>